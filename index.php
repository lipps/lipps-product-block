<?php
/**
 * Plugin Name: Lipps Product Block
 * Description: Custom block for Lipps product
 * Version: 1.0.6
 * Author: Neulab Inc.
 *
 * @package lipps-product-block
 */

defined( 'ABSPATH' ) || exit;

function lipps_product_render_callback( $attributes ) {
	$item_list = '';

	$align                    = array_key_exists( 'align', $attributes ) ? 'align' . $attributes['align'] : '';
	$local_anchor             = array_key_exists( 'local_anchor', $attributes ) ? $attributes['local_anchor'] : '';
	$subtitle                 = array_key_exists( 'product_subtitle', $attributes ) ? $attributes['product_subtitle'] : '';
	$title                    = array_key_exists( 'product_title', $attributes ) ? $attributes['product_title'] : '';
	$title_background_color   = array_key_exists( 'title_background_color', $attributes ) ? $attributes['title_background_color'] : '#fff';
	$title_font_color         = array_key_exists( 'title_font_color', $attributes ) ? $attributes['title_font_color'] : '#969696';
	$main_font_color          = array_key_exists( 'main_font_color', $attributes ) ? $attributes['main_font_color'] : '#4f4f4f';
	$product_background_color = array_key_exists( 'product_background_color', $attributes ) ? $attributes['product_background_color'] : '#f5f5f5';
	$detail_button_color      = array_key_exists( 'detail_button_color', $attributes ) ? $attributes['detail_button_color'] : '#9f916d';

	$title_html = '';
	if ( !empty($subtitle) ) {
		$title_html .= '<div class="new-available" style="color:'.$title_font_color.'">'.$subtitle.'</div>';
	}
	if ( !empty($title) ) {
		$title_html .= '<h2 class="main-content-heading" style="color:'.$title_font_color.'">'.$title.'</h2>';
	}

	if ( !empty($local_anchor) ) {
		$local_anchor = 'id="'.$local_anchor.'"';
	}

	$product_colors = array(
		'font_color'   => $main_font_color,
		'button_color' => $detail_button_color,
	);

	if ( array_key_exists( 'category', $attributes ) ) {
		$args = array(
			'numberposts' => - 1,
			'post_type'   => 'product',
			'category'    => $attributes['category'],
		);

		$items = get_posts( $args );
		if ( ! empty( $items ) ) {
			foreach ( $items as $item ) {
				$item_list .= lipps_product_get_item_description( $item, $product_colors );
			}
		}
	}

	return sprintf( '
      <style>
        .w-slider-dot {
          background: rgba(0, 0, 0, 0.2);
        }
        .w-slider-dot.w-active {
          background: rgba(0, 0, 0, 0.6);
        }
        .button {
          color: %5$s;
          border: 0.5px solid %5$s;
        }
        .button:hover {
          background-color: %5$s;
          color: #fff;
        }
      </style>

      <div %1$s class="lipps-product-title-container %7$s" style="background-color: %6$s">
        <div class="w-container">%2$s</div>
      </div>
      <div class="lipps-product-container %7$s" style="background-color: %3$s">
        <div class="w-container">
          <div class="lipps-boy-list">%4$s</div>
        </div>
      </div>
			', $local_anchor, $title_html, $product_background_color, $item_list, $product_colors['button_color'], $title_background_color, $align
	);
}

function lipps_product_get_item_description( $item, $product_colors ) {
	$name = $item->post_title;

	$values    = get_post_meta( $item->ID );
	$variation = str_replace( ' ', '<br>', $values['lip_variation'][0] );

	$slider = '';
	if ( array_key_exists( 'lip_gallery', $values ) ) {
		$image_id_array = explode( ',', $values['lip_gallery'][0] );
		if ( count( $image_id_array ) > 1 ) {
			$slider .= '<div class="lipps-boy-slider w-slider"><div class="w-slider-mask">';
			foreach ( $image_id_array as $image_id ) {
				$url    = wp_get_attachment_url( $image_id );
				$slider .= lipps_product_set_image( $url );
			}
			$slider .= '</div>';
		} else {
			$url    = wp_get_attachment_url( $image_id_array[0] );
			$slider .= '<div class="lipps-boy-slider w-slider no-slide">';
			$slider .= lipps_product_set_image( $url );
		}
		$slider .= '<div class="lipps-boy-slider-nav w-slider-nav w-round"></div></div>';
	} else {
		$slider .= '<div class="lipps-boy-slider w-slider no-slide">';
		$url    = plugins_url() . '/lipps-product-block/images/no_image.png';
		$slider .= lipps_product_set_image( $url );
		$slider .= '<div class="lipps-boy-slider-nav w-slider-nav w-round"></div></div>';
	}

	$tag = '';
	if ( ! empty( get_the_tags( $item->ID ) ) ) {
		$tag = get_the_tags( $item->ID )[0]->name;
	}

	$slug = get_post_field( 'post_name' , $item->ID );

	$tag_description = '';
	if ( ! empty( $values['lip_colors'][0] ) ) {
		$tag_description .= '<div class="lipps-boy-specs-color-wrap">';
		$tag_description .= '<p class="lipps-boy-specs-paragraph category">' . $tag . '</p>';
		$tag_description .= '<ul class="lipps-boy-specs-color">';
		$color_id        = unserialize( $values['lip_colors'][0] );
		if ( is_array( $color_id ) ) {
			foreach ( $color_id as $id ) {
				$color           = get_post_meta( $id );
				$tag_description .= '<li class="lipps-boy-specs-color-palette" style="background-color:' . $color['lip_color'][0] . '"></li>';
			}
			$tag_description .= '</ul>';
		} else {
			$tag_description = '<li class="lipps-boy-specs-color-palette" style="background-color:' . get_post_meta( $color_id )['lip_color'][0] . '"></li></ul>';
		}
		$tag_description .= '</div>';
	} else {
		$tag_description .= '<p class="lipps-boy-specs-paragraph category">' . $tag . '</p>';
	}

	$product_description = '';
	if ( ! empty( $values['lip_description'][0] ) ) {
		$product_description = '<p class="lipps-boy-specs-paragraph description">' . $values['lip_description'][0] . '</p>';
	}

	$price = '--';
	if ( ! empty( $values['lip_price_jpy'][0] ) ) {
		$price = number_format( floatval( $values['lip_price_jpy'][0] ) );
	}
	if ( ! empty( $values['lip_price_max_jpy'][0] ) ) {
		$price_max = number_format( floatval( $values['lip_price_max_jpy'][0] ) );
		$price     = $price . ' ~ ' . $price_max;
	}

	$product_url = get_the_permalink( $item->ID );

	return sprintf( '
      <div id="%9$s" class="lipps-boy-item">　
        <div class="lipps-boy-images">%1$s
         <div class="lipps-boy-slider-spacer"></div>
        </div>
        <div class="lipps-boy-specs" style="color:%7$s">
          <p class="lipps-boy-specs-paragraph">%2$s</p>
          <p class="lipps-boy-specs-paragraph">%3$s</p>%4$s%5$s
          <p class="lipps-boy-specs-paragraph price">本体価格%6$s円<span class="lipps-boy-specs-paragraph tax">+税</span></p>
        </div>
        <a href="%8$s" class="button lipps-boy-link-button w-button">詳細</a>
      </div>', $slider, $name, $variation, $tag_description, $product_description, $price, $product_colors['font_color'], $product_url, $slug
	);
}

function lipps_product_set_image( $url ) {
	return '<div class="w-slide"><img src="' . $url . '" srcset="' . $url . ' 500w, ' . $url . ' 640w" sizes="(max-width: 479px) 60vw, (max-width: 767px) 30vw, (max-width: 991px) 145px, 178.66px" alt="" class="lipps-boy-slide-content"></div>';
}

function lipps_product_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'lipps-product-block',
		plugins_url( 'build/index.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
	);

	wp_register_style(
		'lipps-product-block-editor',
		plugins_url( 'editor.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'editor.css' )
	);
	register_block_type( 'lipps/lipps-product-block', array(
		'editor_style'    => 'lipps-product-block-editor',
		'editor_script'   => 'lipps-product-block',
		'render_callback' => 'lipps_product_render_callback'
	) );
}

add_action( 'init', 'lipps_product_register_block' );

?>
