const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;
const { InspectorAdvancedControls, InspectorControls, RichText } = wp.editor;
const { TextControl, ColorPalette, PanelBody } = wp.components;

registerBlockType( 'lipps/lipps-product-block', {
	title: 'LIPPS-商品',
	icon: 'products',
	category: 'layout',
	supports: {
		align: true,
	},
	attributes: {
		product_title: {
			type: 'string',
			default: 'title',
			selector: 'h2',
		},
		product_subtitle: {
			type: 'string',
			default: 'subtitle',
			selector: 'h2',
		},
		category: {
			type: 'string',
			default: 'カテゴリー名',
			selector: 'p',
		},
		detail_button_color: {
			type: 'string',
			selector: 'black',
			default: '#9f916d',
		},
		main_font_color: {
			type: 'string',
			selector: 'black',
			default: '#4f4f4f',
		},
		title_font_color: {
			type: 'string',
			selector: 'black',
			default: '#969696'
		},
		title_background_color: {
			type: 'string',
			selector: 'black',
			default: '#fff'
		},
		product_background_color: {
			type: 'string',
			selector: 'black',
			default: '#f5f5f5',
		},
		local_anchor: {
			type: 'string',
		},
	},

	edit: withSelect( (select ) => {
		return {
			categories: select( 'core' ).getEntityRecords( 'taxonomy', 'category', { per_page: -1 } )
		};
	} ) ( ( props ) => {
		const {attributes: {product_title, product_subtitle, title_background_color, category, detail_button_color, main_font_color, title_font_color, product_background_color, local_anchor}, setAttributes, className, categories} = props;

		if ( ! categories ) {
			return "Loading...";
		}

		if ( categories && categories.length === 0 ) {
			return "No posts";
		}

		let options = categories.map((n) => (
			<option value={n.id}> {n.name} </option>
		));
		options.unshift(<option value=''> ----- 選択してください ----- </option>);

		const onChangeContent = (selected) => {
			setAttributes( {category: selected.target.value} );
		};

		return (
			<div className = {className}>
				<InspectorControls>
					<PanelBody title={'色設定'} initialOpen={ true }>
						<span>タイトルカラー<span className="lipps-product-color-indicator" style={{backgroundColor: title_font_color}}></span></span>
						<ColorPalette
							value={ title_font_color }
							onChange={ (value) => setAttributes( {title_font_color: value === undefined ? '#969696' : value } ) }
						/>
						<span>タイトル背景カラー<span className="lipps-product-color-indicator" style={{backgroundColor: title_background_color}}></span></span>
						<ColorPalette
							value={ title_background_color }
							onChange={ (value) => setAttributes( {title_background_color: value === undefined ? '#fff' : value } ) }
						/>
						<span>フォントカラー<span className="lipps-product-color-indicator" style={{backgroundColor: main_font_color}}></span></span>
						<ColorPalette
							value={ main_font_color }
							onChange={ (value) => setAttributes( {main_font_color: value  === undefined ? '#969696' : value } ) }
						/>
						<span>背景カラー<span className="lipps-product-color-indicator" style={{backgroundColor: product_background_color}}></span></span>
						<ColorPalette
							value={ product_background_color }
							onChange={ (value) => setAttributes( {product_background_color: value === undefined ? '#f5f5f5' : value } ) }
						/>
						<span>詳細ボタンカラー<span className="lipps-product-color-indicator" style={{backgroundColor: detail_button_color}}></span></span>
						<ColorPalette
							value={ detail_button_color }
							onChange={ (value) => setAttributes( {detail_button_color: value === undefined ? '#9f916d' : value } ) }
						/>
					</PanelBody>
				</InspectorControls>
				<select onChange={onChangeContent}> {options} </select>
				<div className="title-container" style={{backgroundColor: title_background_color}}>
					<div className = "container" >
						<RichText
							tagNmae="h2"
							className="product-title"
							style={{color:title_font_color}}
							value={ product_subtitle }
							onChange={ (new_words) => setAttributes( {product_subtitle: new_words} ) }
						/>
						<RichText
							tagNmae="h2"
							className="product-title"
							style={{color:title_font_color}}
							value={ product_title }
							onChange={ (new_words) => setAttributes( {product_title: new_words} ) }
						/>
					</div>
				</div>
				<InspectorAdvancedControls>
					<TextControl
						label="HTML アンカー"
						value={local_anchor}
						onChange={ (new_anchor) => setAttributes( {local_anchor: new_anchor} ) }
					/>
				</InspectorAdvancedControls>

				<div class="outer-container" style={{backgroundColor: product_background_color}}>
					<div class="container">
						<div className="product-list">
							<div className="item">
								<div className="product-image-container">
									<img src="../wp-content/plugins/lipps-product-block/images/sample_spotcover.png" alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button" style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイスポットカバー</p>
									<p className="product-variation">#001(明るい色)<br/>#002(自然な肌色)</p>
									<div className="color-container">
										<p className="product-tag">コンシーラー</p>
										<ul className="color-list">
											<li className="color-palette" style={{background: "#ebcd96"}}></li>
											<li className="color-palette" style={{background: "#daac8e"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,000円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{color:"#fff", backgroundColor: detail_button_color, border: "0.5px solid "+detail_button_color }}>詳細</div>
							</div>
							<div className="item">
								<div className="product-image-container">
									<img src="../wp-content/plugins/lipps-product-block/images/sample_facegel.png" alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button" style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイ フェイスジェル</p>
									<p className="product-variation">#002</p>
									<div className="color-container">
										<p className="product-tag">BBクリーム</p>
										<ul className="color-list">
											<li className="color-palette" style={{background: "#daac8e"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,200円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{color:detail_button_color, border: "0.5px solid "+detail_button_color }}>詳細</div>
							</div>
							<div className="item">
								<div className="product-image-container">
									<img src="../wp-content/plugins/lipps-product-block/images/sample_facepowder.png" alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button" style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイ フェイスパウダー</p>
									<p className="product-variation">#001(明るい色)</p>
									<div className="color-container">
										<p className="product-tag">ファンデーション</p>
										<ul className="color-list">
											<li className="color-palette" style={{background: "#ebcd96"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,600円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{color:detail_button_color, border: "0.5px solid "+detail_button_color }}>詳細</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	} ),
	save: (props) => {
		const {attributes: {product_title, product_subtitle, title_background_color, category, detail_button_color, main_font_color, title_font_color, product_background_color, local_anchor}, setAttributes, className, categories} = props;
		return (
			<div className={className}>
				<div className="title-container" style={{backgroundColor: title_background_color}}>
					<div className="container">
						<RichText.Content tagName="h2" value={product_subtitle} />
						<RichText.Content tagName="h2" value={product_title} />
					</div>
				</div>
				<div className="outer-container"
					 style={{backgroundColor: product_background_color}}>
					<div className="container">
						<div className="product-list">
							<div className="item">
								<div className="product-image-container">
									<img
										src="../wp-content/plugins/lipps-product-block/images/sample_spotcover.png"
										alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button"
										 style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイスポットカバー</p>
									<p className="product-variation">#001(明るい色)<br/>#002(自然な肌色)</p>
									<div className="color-container">
										<p className="product-tag">コンシーラー</p>
										<ul className="color-list">
											<li className="color-palette"
												style={{background: "#ebcd96"}}></li>
											<li className="color-palette"
												style={{background: "#daac8e"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,000円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{
									color: "#fff",
									backgroundColor: detail_button_color,
									border: "0.5px solid " + detail_button_color
								}}>詳細
								</div>
							</div>
							<div className="item">
								<div className="product-image-container">
									<img
										src="../wp-content/plugins/lipps-product-block/images/sample_facegel.png"
										alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button"
										 style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイ フェイスジェル</p>
									<p className="product-variation">#002</p>
									<div className="color-container">
										<p className="product-tag">BBクリーム</p>
										<ul className="color-list">
											<li className="color-palette"
												style={{background: "#daac8e"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,200円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{
									color: detail_button_color,
									border: "0.5px solid " + detail_button_color
								}}>詳細
								</div>
							</div>
							<div className="item">
								<div className="product-image-container">
									<img
										src="../wp-content/plugins/lipps-product-block/images/sample_facepowder.png"
										alt="" className="product-image"/>
								</div>
								<div className="slider-button-container">
									<div className="slider-button"
										 style={{background: "rgba(0,0,0,0.6)"}}></div>
									<div className="slider-button"></div>
								</div>
								<div className="specification" style={{color: main_font_color}}>
									<p className="product-name">リップスボーイ フェイスパウダー</p>
									<p className="product-variation">#001(明るい色)</p>
									<div className="color-container">
										<p className="product-tag">ファンデーション</p>
										<ul className="color-list">
											<li className="color-palette"
												style={{background: "#ebcd96"}}></li>
										</ul>
									</div>
									<p className="product-price">本体価格2,600円</p>
								</div>
								<div className="lipps-boy-link-button w-button" style={{
									color: detail_button_color,
									border: "0.5px solid " + detail_button_color
								}}>詳細
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
} );
